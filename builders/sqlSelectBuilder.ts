import TranslationError from "../errors/TranslationError";
import { TCondition } from "../transformers/types";

const formatField = (field: unknown) => `${field}`.replace(/\./g, "->");

const buildFileds = (fields: string[]) =>
  `SELECT ${fields.map(formatField).join(", ") || "*"}`;

const buildComparisonOperator = ({
  operator,
  operands: [field, value],
}: TCondition) =>
  `${formatField(field)} ${operator.toUpperCase()} ${JSON.stringify(value)}`;

const buildListOperator = ({
  operator,
  operands: [field, array],
}: TCondition) =>
  Array.isArray(array)
    ? `${formatField(field)} ${operator.toUpperCase()} (${array
        .map((value) => JSON.stringify(value))
        .join(", ")})`
    : "";

const buildLogicalOperator = ({ operator, operands }: TCondition) =>
  operands
    .map((operand) => {
      if (!operand || typeof operand !== "object" || Array.isArray(operand)) {
        throw new TranslationError();
      }

      return buildOperator(operand);
    })
    .filter(Boolean)
    .map((operator) => `(${operator})`)
    .join(` ${operator.toUpperCase()} `);

const buildOperator = (constraint: TCondition): string => {
  if (["<", "<=", ">", ">=", "=", "!="].includes(constraint.operator)) {
    return buildComparisonOperator(constraint);
  }

  if (["in", "not in"].includes(constraint.operator)) {
    return buildListOperator(constraint);
  }

  return buildLogicalOperator(constraint);
};

const buildWhere = (constraint: TCondition) => {
  if (!constraint.operands.length) return "";

  const constraints = buildOperator(constraint);

  return constraints && `WHERE ${constraints}`;
};

export const buildSqlSelect = (
  table: string,
  fields: string[],
  constraints: TCondition
) => {
  return (
    `${buildFileds(fields)} FROM ${table} ${buildWhere(constraints)}`.trim() +
    ";"
  );
};
