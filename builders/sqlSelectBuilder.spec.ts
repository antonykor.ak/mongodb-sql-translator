import { buildSqlSelect } from "./sqlSelectBuilder";

describe("buildSqlSelect", () => {
  it("should insert table name", () => {
    expect(
      buildSqlSelect("test", [], { operator: "and", operands: [] })
    ).toContain("FROM test");
  });

  it("should insert * if fields array is empty", () => {
    expect(
      buildSqlSelect("table", [], { operator: "and", operands: [] })
    ).toContain("SELECT *");
  });

  it("should insert fields if fields array is empty", () => {
    expect(
      buildSqlSelect("table", ["name", "age"], {
        operator: "and",
        operands: [],
      })
    ).toContain("SELECT name, age");
  });

  it("should not add WHERE if constraints are empty", () => {
    expect(
      buildSqlSelect("table", [], { operator: "and", operands: [] })
    ).not.toContain("WHERE");
  });

  describe("if constraints are not empty", () => {
    it("should use quotes for strings", () => {
      expect(
        buildSqlSelect("table", [], {
          operator: "=",
          operands: ["name", "john"],
        })
      ).toContain('WHERE name = "john"');
    });

    it.each(["<", "<=", ">", ">=", "=", "!="])(
      "should add %s if it was used in constraints",
      (operator) => {
        expect(
          buildSqlSelect("table", [], { operator, operands: ["age", 20] })
        ).toContain(`WHERE age ${operator} 20`);
      }
    );

    it.each`
      operator    | sqlOperator
      ${"in"}     | ${"IN"}
      ${"not in"} | ${"NOT IN"}
    `(
      "should add $sqlOperator operator if $operator was used in constraints",
      ({ operator, sqlOperator }) => {
        expect(
          buildSqlSelect("table", [], {
            operator,
            operands: ["age", [20, 30]],
          })
        ).toContain(`WHERE age ${sqlOperator} (20, 30)`);
      }
    );

    it.each`
      operator | sqlOperator
      ${"or"}  | ${"OR"}
      ${"and"} | ${"AND"}
    `(
      "should add $sqlOperator operator if $operator was used in constraints",
      ({ operator, sqlOperator }) => {
        expect(
          buildSqlSelect("table", ["name", "age"], {
            operator,
            operands: [
              { operator: "=", operands: ["age", 10] },
              { operator: ">=", operands: ["age", 20] },
            ],
          })
        ).toContain(`WHERE (age = 10) ${sqlOperator} (age >= 20)`);
      }
    );
  });

  it("should format nested fields", () => {
    expect(
      buildSqlSelect("table", ["user.name"], {
        operator: "=",
        operands: ["age", 10],
      })
    ).toContain("user->name");
    expect(
      buildSqlSelect("table", [], {
        operator: "=",
        operands: ["user.age", 10],
      })
    ).toContain("user->age");
  });
});
