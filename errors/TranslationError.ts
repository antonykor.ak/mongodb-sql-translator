export default class TranslationError extends Error {
  constructor(message = "Invalid format") {
    super(message);
  }
}
