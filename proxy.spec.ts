import db from "./proxy";

describe("proxy", () => {
  test("example 1", () => {
    expect(db.user.find({ name: "john" })).toEqual(
      'SELECT * FROM user WHERE name = "john";'
    );
  });

  test("example 2", () => {
    expect(db.user.find({ _id: 23113 }, { name: 1, age: 1 })).toEqual(
      "SELECT name, age FROM user WHERE _id = 23113;"
    );
  });

  test("example 3", () => {
    expect(db.user.find({ age: { $gte: 21 } }, { name: 1, _id: 1 })).toEqual(
      "SELECT name, _id FROM user WHERE age >= 21;"
    );
  });
});
