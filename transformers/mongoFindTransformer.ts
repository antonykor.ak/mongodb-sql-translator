import TranslationError from "../errors/TranslationError";
import {
  ArrayOfPrimitives,
  Primitive,
  Projection,
  Queries,
  Query,
  TCondition,
  TObjectField,
  TPrimitive,
  TQuery,
} from "./types";

const addPrefix = (key: string, prefix: string) =>
  prefix ? `${prefix}.${key}` : key;

export const transformProjection = (
  projection: unknown,
  prefix = ""
): string[] => {
  if (!Projection.guard(projection)) {
    throw new TranslationError("Invalid projection format");
  }

  return Object.entries(projection)
    .filter(([, value]) => value)
    .flatMap(([key, value]) =>
      typeof value === "object"
        ? transformProjection(value, addPrefix(key, prefix))
        : addPrefix(key, prefix)
    );
};

const operatorsMap: Record<string, string> = {
  $gt: ">",
  $gte: ">=",
  $lt: "<",
  $lte: "<=",
  $eq: "=",
  $ne: "!=",
  $in: "in",
  $nin: "not in",
  $or: "or",
  $and: "and",
};

const transformLogicalOperator = (
  operator: "$or" | "$and",
  value: TQuery[],
  prefix = ""
): TCondition =>
  value.length > 1
    ? {
        operator: operatorsMap[operator],
        operands: value.map((item) => transformQuery(item, prefix)),
      }
    : transformQuery(value[0], prefix);

const tranformField = (
  field: string,
  value: TPrimitive | TObjectField,
  prefix = ""
): TCondition => {
  if (Primitive.guard(value))
    return {
      operator: "=",
      operands: [addPrefix(field, prefix), value],
    };

  const constraints = Object.entries(value).map(
    ([operator, value]): TCondition => ({
      operator: operatorsMap[operator],
      operands: [addPrefix(field, prefix), value],
    })
  );

  if (constraints.length === 1) return constraints[0];

  return {
    operator: "and",
    operands: constraints,
  };
};

export const transformQuery = (query: unknown, prefix = ""): TCondition => {
  if (!Query.guard(query)) throw new TranslationError("Invalid query format");

  const constraints = Object.entries(query).flatMap(([key, value]) => {
    if ((key === "$or" || key === "$and") && Queries.guard(value)) {
      return transformLogicalOperator(key, value, prefix);
    }

    if (Query.guard(value)) {
      return transformQuery(value, addPrefix(key, prefix));
    }

    if (ArrayOfPrimitives.guard(value)) {
      return value.map((item, index) =>
        tranformField(`${index}`, item, addPrefix(key, prefix))
      );
    }

    if (Array.isArray(value) || value === undefined) {
      throw new TranslationError("Invalid query format");
    }

    return tranformField(key, value, prefix);
  });

  if (constraints.length === 1) return constraints[0];

  return {
    operator: "and",
    operands: constraints,
  };
};
