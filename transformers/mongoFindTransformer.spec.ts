import TranslationError from "../errors/TranslationError";
import { transformProjection, transformQuery } from "./mongoFindTransformer";

describe("transformProjection", () => {
  it("should throw error if query string is in invalid format", () => {
    expect(() => transformProjection({ _id: false })).toThrow(TranslationError);
  });

  it("should return array of fields with 1", () => {
    expect(transformProjection({ _id: 0, name: 1 })).toEqual(["name"]);
  });

  it("should support subdocuments", () => {
    expect(transformProjection({ size: { uom: 1, a: { b: 1 } } })).toEqual([
      "size.uom",
      "size.a.b",
    ]);
  });
});

describe("transformQuery", () => {
  it("should throw error if query string is in invalid format", () => {
    expect(() => transformQuery({ age: { $gte: [10] } })).toThrow(
      TranslationError
    );
  });

  it("should map fields to and operation", () => {
    expect(transformQuery({ age: 10, name: "john" })).toEqual({
      operator: "and",
      operands: [
        { operator: "=", operands: ["age", 10] },
        { operator: "=", operands: ["name", "john"] },
      ],
    });
  });

  it.each`
    mongoOperator | operator
    ${"$lt"}      | ${"<"}
    ${"$lte"}     | ${"<="}
    ${"$gt"}      | ${">"}
    ${"$gte"}     | ${">="}
    ${"$eq"}      | ${"="}
    ${"$ne"}      | ${"!="}
  `(
    "should map $mongoOperator comparison operator to $operator",
    ({ mongoOperator, operator }) => {
      expect(transformQuery({ age: { [mongoOperator]: 10 } })).toEqual({
        operator: operator,
        operands: ["age", 10],
      });
    }
  );

  it.each`
    mongoOperator | operator
    ${"$in"}      | ${"in"}
    ${"$nin"}     | ${"not in"}
  `(
    "should map $mongoOperator list operator to $operator",
    ({ mongoOperator, operator }) => {
      expect(transformQuery({ age: { [mongoOperator]: [10, 15] } })).toEqual({
        operator: operator,
        operands: ["age", [10, 15]],
      });
    }
  );

  it("should allow compination of comparison operators", () => {
    expect(transformQuery({ age: { $gt: 10, $lte: 20 } })).toEqual({
      operator: "and",
      operands: [
        { operator: ">", operands: ["age", 10] },
        { operator: "<=", operands: ["age", 20] },
      ],
    });
  });

  it.each`
    mongoOperator | operator
    ${"$or"}      | ${"or"}
    ${"$and"}     | ${"and"}
  `(
    "should map $mongoOperator logical operator to $operator",
    ({ mongoOperator, operator }) => {
      expect(
        transformQuery({ [mongoOperator]: [{ age: 10 }, { name: "john" }] })
      ).toEqual({
        operator: operator,
        operands: [
          { operator: "=", operands: ["age", 10] },
          { operator: "=", operands: ["name", "john"] },
        ],
      });
    }
  );

  it("should support inherited logical operators", () => {
    expect(
      transformQuery({
        $or: [{ $and: [{ name: "john" }, { age: 10 }] }, { age: 20 }],
      })
    ).toEqual({
      operator: "or",
      operands: [
        {
          operator: "and",
          operands: [
            { operator: "=", operands: ["name", "john"] },
            { operator: "=", operands: ["age", 10] },
          ],
        },
        { operator: "=", operands: ["age", 20] },
      ],
    });
  });

  it("should support subdocuments", () => {
    expect(transformQuery({ size: { h: 14, w: 21, uom: "cm" } })).toEqual({
      operator: "and",
      operands: [
        { operator: "=", operands: ["size.h", 14] },
        { operator: "=", operands: ["size.w", 21] },
        { operator: "=", operands: ["size.uom", "cm"] },
      ],
    });
  });

  it("should support arrays", () => {
    expect(transformQuery({ tags: ["red", "blank"] })).toEqual({
      operator: "and",
      operands: [
        { operator: "=", operands: ["tags.0", "red"] },
        { operator: "=", operands: ["tags.1", "blank"] },
      ],
    });
  });
});
