import {
  Array,
  Boolean,
  Dictionary,
  Lazy,
  Literal,
  Null,
  Number,
  Partial,
  Runtype,
  Static,
  String,
  Union,
} from "runtypes";

type TProjection = { [_: string]: 0 | 1 | TProjection };
export const Projection: Runtype<TProjection> = Lazy(() =>
  Dictionary(Literal(0).Or(Literal(1)).Or(Projection), String)
);

export const Primitive = Union(String, Number, Boolean, Null);
export type TPrimitive = Static<typeof Primitive>;
export const ArrayOfPrimitives = Array(Primitive);

const ObjectField = Partial({
  $gt: Primitive,
  $gte: Primitive,
  $lt: Primitive,
  $lte: Primitive,
  $eq: Primitive,
  $ne: Primitive,
  $in: ArrayOfPrimitives,
  $nin: ArrayOfPrimitives,
});
export type TObjectField = Static<typeof ObjectField>;

export type TQuery = {
  [key: string]:
    | TQuery[]
    | TQuery
    | TObjectField
    | TPrimitive
    | TPrimitive[]
    | undefined;
  $or?: TQuery[];
  $and?: TQuery[];
};
export const Query: Runtype<TQuery> = Lazy(() =>
  Dictionary(
    Array(Query).Or(Query).Or(ObjectField).Or(Primitive).Or(ArrayOfPrimitives),
    String
  ).And(Partial({ $or: Array(Query), $and: Array(Query) }))
).withConstraint(
  (obj) =>
    !Object.keys(obj).find(
      (key) => key[0] === "$" && !["$or", "$and"].includes(key)
    )
);
export const Queries = Array(Query);

type TCompareCondition = {
  operator: string;
  operands: [TPrimitive, TPrimitive | TPrimitive[]];
};
export type TCondition =
  | TCompareCondition
  | {
      operator: string;
      operands: TCondition[];
    };
