# MongoDB to SQL translator

## Ways of usage:

1. CLI playground

To run the interactive playground use `yarn playground`

```
user@pc mongodb-sql-translator % yarn playground
yarn run v1.22.17
$ ts-node ./cli/playground.ts
in: db.user.find({name: 'john'});
out:  SELECT * FROM user WHERE name = "john";
in: db.user.find({_id: 23113},{name: 1, age: 1});
out:  SELECT name, age FROM user WHERE _id = 23113;
in: db.user.find({age: {$gte: 21}},{name: 1, _id: 1});
out:  SELECT name, _id FROM user WHERE age >= 21;
```

2. db object

`db` object can be imported from `main.ts` and used in the same way as in MongoDB Shell:

```ts
import db from "./main";

const sql = db.inventory.find(
  { "size.h": { $lt: 15 }, "size.uom": "in", status: "D" },
  { size: { uom: 1 } }
);
console.log(sql); // 'SELECT size->uom FROM inventory WHERE (size->h < 15) AND (size->uom = "in") AND (status = "D");'
```

3. Translate function

`translateMongoToSql` function can be imported from `main.ts` and be called with query string.

```ts
import { translateMongoToSql } from "./main";

const sql = translateMongoToSql(
  'db.inventory.find( { tags: ["red", "blank"] } );'
);
console.log(sql); // 'SELECT * FROM inventory WHERE (tags->0 = "red") AND (tags->1 = "blank");'
```

> Support of subdocuments and arrays is limited due to uncertainty of data scheme.
