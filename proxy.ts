import { buildSqlSelect } from "./builders/sqlSelectBuilder";
import TranslationError from "./errors/TranslationError";
import {
  transformProjection,
  transformQuery,
} from "./transformers/mongoFindTransformer";

const db: Record<
  string,
  { find: (query: unknown, projection?: unknown) => string }
> = new Proxy(
  {},
  {
    get(_, prop) {
      return {
        find(query: unknown, projection: unknown = {}) {
          if (typeof prop !== "string") {
            throw new TranslationError("Invalid collection");
          }

          return buildSqlSelect(
            prop,
            transformProjection(projection),
            transformQuery(query)
          );
        },
      };
    },
  }
);

export default db;
