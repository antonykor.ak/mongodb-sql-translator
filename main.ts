import { parseMongoFind } from "./parsers/mongoFindParser";
import db from "./proxy";

export const translateMongoToSql = (queryString: string) => {
  const { collection, query, projection } = parseMongoFind(queryString);

  return db[collection].find(query, projection);
};

export default db;
