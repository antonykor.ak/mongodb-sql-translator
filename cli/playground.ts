import readline from "node:readline";
import TranslationError from "../errors/TranslationError";
import { translateMongoToSql } from "../main";

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const askQuestion = (question: string) =>
  new Promise<string>((res) => rl.question(question, res));

const loop = async () => {
  // eslint-disable-next-line no-constant-condition
  while (true) {
    try {
      const query = await askQuestion("in: ");

      console.log("out: ", translateMongoToSql(query));
    } catch (error) {
      if (error instanceof TranslationError) {
        console.log(error.message);
      } else {
        console.error(error);
      }
    }
  }
};

loop();
