import JSON5 from "json5";
import TranslationError from "../errors/TranslationError";

export const parseMongoFind = (queryString: string) => {
  const match = queryString.trim().match(/^db\.(.+)\.find\((.+)\);?$/);

  if (!match) throw new TranslationError();

  const [, collection, args] = match;

  try {
    const [query, projection = {}] = JSON5.parse<unknown[]>(`[${args}]`);

    return { collection, query, projection };
  } catch {
    throw new TranslationError();
  }
};
