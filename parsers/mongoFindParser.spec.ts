import TranslationError from "../errors/TranslationError";
import { parseMongoFind } from "./mongoFindParser";

describe("parseMongoFind", () => {
  it("should throw error if query string is in invalid format", () => {
    expect(() => parseMongoFind("invalid")).toThrow(TranslationError);
  });

  it("should throw error if query string does't contain contain collection name", () => {
    expect(() => parseMongoFind("db.find({});")).toThrow(TranslationError);
  });

  it("should throw error if operation is not find", () => {
    expect(() => parseMongoFind("db.col.findOne({});")).toThrow(
      TranslationError
    );
  });

  it("should throw error if query parameter is missing", () => {
    expect(() => parseMongoFind("db.col.find();")).toThrow(TranslationError);
  });

  it("should throw error if query parameter is not valid js", () => {
    expect(() => parseMongoFind("db.col.find(asse);")).toThrow(
      TranslationError
    );
  });

  it("should throw error if projection parameter is not valid js", () => {
    expect(() => parseMongoFind("db.col.find({},asse);")).toThrow(
      TranslationError
    );
  });

  it("should return collection name", () => {
    expect(parseMongoFind("db.col.find({});")).toHaveProperty(
      "collection",
      "col"
    );
  });

  it("should return query parameter", () => {
    expect(parseMongoFind("db.col.find({name: 'john'});")).toHaveProperty(
      "query",
      { name: "john" }
    );
  });

  it("should return projection parameter if it's present", () => {
    expect(
      parseMongoFind("db.col.find({name: 'john'},{name: 1, age: 1});")
    ).toHaveProperty("projection", { name: 1, age: 1 });
  });

  it("should return empty object for projection parameter  if it isn't present", () => {
    expect(parseMongoFind("db.col.find({name: 'john'});")).toHaveProperty(
      "projection",
      {}
    );
  });
});
